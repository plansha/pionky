# Chronologia impresywna

> >Chcialbym moc siedziec dzis do zachodu slonca. I jeszcze chwilke, pare minut po tym jak sie sciemni. Tak nie za dlugo, by nie zgubic tych cennych kilku godzin snu, ktore doceniac ucze sie dopiero gdy mi ich brak. Brak ogonkow utrudnia wyrazanie mysli, ale to czy nie na tym polega brak czasu - by braklo go na te najbardziej potrzebne pierdoly? Wlasnie dlatego, chcialbym dzis liczyc czas w promieniach slonca. Nie minutach, nie godzinach, czy spotkaniach, ale w tych zludnych okresleniach - bo czymze innym nie jest szczypta czasu, jak paroma chwilami, ktore tak trudno czasem docenic?"

Po tej emocjonalnej etiudzie rozegranej przez jego place na ekranie komputera, odetchnął z ulgą. Jeszcze tylko przez moment stał w zadumie patrząc przez okno w ostatnie błyski odchodzącego już dnia.

 _"Nawet nie zainstalowali modułu znaków diaktrycznych. Idioci. Wszędzie idioci._ - Westchnął beznadziejnie, otrząsnąwszy się z marazmu myśli.

Rozpoczynała się noc, a temperatura i współczynnik radioaktywności spadły do poziomu czwartego w skali Phersona. Nieubłaganie nadszedł czas walki o przetrwanie kolejnej doby. Wreszcie mógł opuścić budynek by udać się na zewnątrz.

Zrazu wszystkie oznaki tzw. "człowieczeństwa", jakie tliły się w nim gdy siedział przed ekranem komputera, zanikły. To odwieczna prawda znana najświetlejszym filozofom, że człowiek zamyślony staje się nieszczęśliwy. Zbyt długi czas oddany zadumie nie sprzyja miłym i pogodnym nastrojom, ponieważ natura stworzyła człowieka bestią, mającą zapewnić sobie przetrwanie. A cóż innego stoi na przeszkodzie przedłużeniu swego gatunku, jak nie klasyczny *problem*, który tak łatwo można odnaleźć dając odpłynąć swym myślom ku nieznanym horyzontom podświadomości?

Jednakże teraz bestia wyszła polować, a horyzont jawił się kolorem fioletowym. Podwyższona zawartość argonu w atmosferze połączona z silnymi burzami magnetycznymi sprawiała wrażenie jakby cała pobliska okolica przykryta była neonowym całunem. Nieustannie padający kwaśny deszcz nadawał tej okolicy miłe wrażenie, budząc złudne poczucie bezpieczeństwa, że przynajmniej o brak wody nie należy się martwić. O jeden problem mniej...

